# ng-movies

Interview test

[![Hooligan Development](https://hooligan.co.za/Assets/Common/Hooligan-Development_site_logo.png)](https://hooligan.co.za)

Congratulations! Your at the all importatn final step in the interview process here at Hooligan Development.
Please note that by no means do we expect you to complete the full evalution. This technical evalution is exactly that! An evaluation in order to see where you are in your skill set!

## Methodologies

  - [BEM](http://http://getbem.com)
  - [Idiomatic HTML](https://github.com/necolas/idiomatic-html/blob/master/README.md)
  - [Idiomatic CSS](https://github.com/necolas/idiomatic-css)

## Tech

* [SASS](http://sass-lang.com/) - Looks better than CSS
* [Bootstrap](https://getbootstrap.com/) - (v4) Mostly the grid and utilities 
* [Node](https://nodejs.org/) - Use of NPM or Yarn for our packages and build system
* [TypeScript](https://www.typescriptlang.org/) - For structure and better code
* [Angular](https://angular.io/) - Possibility of using on complex components

## Resources

* [JSON Data](https://github.com/prust/wikipedia-movie-data) - Data for the evaluation

## The Question

Using the https://www.themoviedb.org API. Create a new Angular 2-8 project using the CLI.

### API Docs
https://developers.themoviedb.org/3/getting-started/introduction

### API Key (v3 auth) 
1e859dc9f92ec822aa44af583dfc67fc

### Example API Request 
https://api.themoviedb.org/3/movie/550?api_key=1e859dc9f92ec822aa44af583dfc67fc 

### API Read Access Token (v4 auth) 
eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxZTg1OWRjOWY5MmVjODIyYWE0NGFmNTgzZGZjNjdmYyIsInN1YiI6IjVkYmFhZjFlNTRmNmViMDAxODRjM2EyMCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.IDUhVhZ_i1laaoe8Wn-ev-0OyHYOh56VsmmzF527cNw

You can use a third party library like https://github.com/cavestri/themoviedb-javascript-library for queries.

### This application must do the following:

* Have a landing page that displays the latest upcoming movies, and the highest rated movies for 2019 and 2018.
* Create a top navigation header.
* List now playing movies.
* Search for a movie by title and display the details in a modal dialog or side panel.
* List the best rated movies for different genres.
* All movies being displayed must be in a poster format with movie poster image. The
* A movie poster must be clickable and display the details in a modal dialog or side panel.

### Bonus:

When viewing the details of the movie include the actor images, imdb ratings and any other additional metadata.

### Finishing up

When you have finished please commit to the repository with an appropriate commit message.